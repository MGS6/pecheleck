﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PechValues.aspx.cs" Inherits="PechRachet.PechValues" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>

<body style="background-color: silver">
    <form id="form1" runat="server">
    <div style="font-size: x-large; font-weight: lighter; font-style: italic; font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif; ">
    
        Тепловой баланас электрической сушильной печи</div>
        <p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Мощность печи (кВт):&nbsp;
            <asp:TextBox ID="TextBox1" runat="server" Width="155px">210</asp:TextBox>
        </p>
        <p>
&nbsp;&nbsp;&nbsp; Температура нагретого воздуха (°с):&nbsp;
            <asp:TextBox ID="TextBox2" runat="server" Width="155px">700</asp:TextBox>
        </p>
        <p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Понижение воздуха (°с):&nbsp;
            <asp:TextBox ID="TextBox3" runat="server" Width="155px">20</asp:TextBox>
        </p>
        <p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Количество нагревателей (шт):&nbsp;
            <asp:TextBox ID="TextBox4" runat="server" Width="155px">6</asp:TextBox>
        </p>
        <p>
&nbsp; Скорость воздуха в калорифере (м/с):&nbsp;
            <asp:TextBox ID="TextBox5" runat="server" Width="155px">10</asp:TextBox>
        </p>
        <p>
&nbsp;&nbsp; Диаметр проволоки калорифера (м):&nbsp;
            <asp:TextBox ID="TextBox6" runat="server" Width="155px">0,004</asp:TextBox>
        </p>
        <p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Напряжение в сети (В):&nbsp;
            <asp:TextBox ID="TextBox7" runat="server" Width="155px">380</asp:TextBox>
        </p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="Button2" runat="server" BackColor="#6666FF" BorderStyle="Double" Font-Size="Medium" Text="Расчет" OnClick="Button2_Click" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="Button1" runat="server" BackColor="#6666FF" BorderStyle="Double" Font-Size="Medium" Text="Исходные данные 2" OnClick="Button1_Click" />
    </form>
    <p>
        &nbsp;</p>
</body>
</html>
