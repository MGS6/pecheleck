﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;

namespace PechRachet.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult CharterHelp()

        {

            new Chart(width: 600, height: 400, theme: ChartTheme.Yellow)

            .AddTitle("Определение кинематической вязкости, м^2/с")

            
            .AddSeries(chartType: "line",

            xValue: new[] { "0", "100", "200", "300", "400", "500", "600", "700", "800", "900", "1000" },

            yValues: new[] { "0.0000132", "0.0000231", "0.0000348", "0.0000482", "0.0000629", "0.0000793", "0.0000967", "0.000115", "0.000135", "0.0001548", "0.0001767" })

            .Write("bmp");

            return null;
        }

        public ActionResult CharterHelp2()

        {

            new Chart(width: 600, height: 400, theme: ChartTheme.Yellow)

            .AddTitle("Определение коэффициента теплопроводности, Вт/м*К")

            .AddSeries(chartType: "line",

            xValue: new[] { "0", "100", "200", "300", "400", "500", "600", "700", "800", "900", "1000", "1100", "1200" },

            yValues: new[] { "0.0243", "0.0319", "0.0387", "0.0448", "0.0505", "0.0562", "0.0615", "0.0666", "0.072", "0.0761", "0.0804", "0.0848", "0.089" })

            .Write("bmp");

            return null;
        }

        public ActionResult CharterHelp3()

        {

            new Chart(width: 600, height: 400, theme: ChartTheme.Yellow)

            .AddTitle("Определение теплоемкости воздуха, кДж/(м^3*К")

            .AddSeries(chartType: "line",

            xValue: new[] { "0", "100", "200", "300", "400", "500", "600", "700", "800", "900", "1000", "1100", "1200", "1300", "1400", "1500", "1600", "1700", "1800", "1900" },

            yValues: new[] { "1.2971", "1.3004", "1.3071", "1.3172", "1.3289", "1.3427", "1.3565", "1.3708", "1.3842", "1.3976", "1.4098", "1.4215", "1.4328", "1.4428", "1.4529", "1.4619", "1.4709", "1.4789", "1.4868", "1.4946" })

            .Write("bmp");

            return null;
        }

        public ActionResult CharterHelp4()

        {

            new Chart(width: 600, height: 400, theme: ChartTheme.Yellow)

            .AddTitle("Определение емкости, кДж/(кг*К)")

            .AddSeries(chartType: "line",

            xValue: new[] { "100", "200", "300", "400", "500", "600", "700", "800", "900", "1000", "1100", "1200", "1300" },

            yValues: new[] { "0.486", "0.498", "0.514", "0.533", "0.555", "0.584", "0.636", "0.703", "0.703", "0.695", "0.691", "0.687", "0.687" })

            .Write("bmp");

            return null;
        }

        public ActionResult CharterHelp5()

        {

            new Chart(width: 600, height: 400, theme: ChartTheme.Yellow)

            .AddTitle("Определение коэффициента k")

            .AddSeries(chartType: "line",

            xValue: new[] { "400", "500", "600", "700", "800", "900"},

            yValues: new[] { "0.4", "0.5", "0.6", "0.7", "0.8", "0.9" })

            .Write("bmp");

            return null;
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}