﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PechRachet.Startup))]
namespace PechRachet
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
