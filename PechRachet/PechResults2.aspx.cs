﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PechLib;
using System.Web.Helpers;

namespace PechRachet
{
    public partial class PechResults2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TextBox55.Text = String.Format("{0:F3}", PechValues2.txtqoa);
            TextBox56.Text = String.Format("{0:F3}", PechValues2.txtlbig);
            TextBox57.Text = String.Format("{0:F3}", PechValues2.txtdbig);
            TextBox58.Text = String.Format("{0:F3}", PechValues2.txthz);
            TextBox59.Text = String.Format("{0:F3}", PechValues2.txtk3);
            TextBox60.Text = String.Format("{0:F3}", PechValues2.txtd0);
            TextBox61.Text = String.Format("{0:F3}", PechValues2.txtnotv);
            TextBox62.Text = String.Format("{0:F3}", PechValues2.txtm);
            TextBox63.Text = String.Format("{0:F3}", PechValues2.txttpol);
            TextBox64.Text = String.Format("{0:F3}", PechValues2.txtkf);
            TextBox65.Text = String.Format("{0:F3}", PechValues2.txtt2dash);
            TextBox66.Text = String.Format("{0:F3}", PechValues2.txtw1c);
            TextBox67.Text = String.Format("{0:F3}", PechValues2.txtw2c);
            TextBox68.Text = String.Format("{0:F3}", PechValues2.txttm2);
            TextBox69.Text = String.Format("{0:F3}", PechValues2.txtqbig);
            TextBox70.Text = String.Format("{0:F3}", PechValues2.txtqbig2);
            TextBox71.Text = String.Format("{0:F3}", PechValues2.txtq5pr);
            TextBox72.Text = String.Format("{0:F3}", PechValues2.txtt0dash);
            TextBox73.Text = String.Format("{0:F3}", PechValues2.txtt1dash);
            TextBox74.Text = String.Format("{0:F3}", PechValues2.txttst1);
            TextBox75.Text = String.Format("{0:F3}", PechValues2.txtsbig);
            TextBox76.Text = String.Format("{0:F3}", PechValues2.txtf1big);
            TextBox77.Text = String.Format("{0:F3}", PechValues2.txtf2big);
            TextBox78.Text = String.Format("{0:F3}", PechValues2.txtfbigsr);
            TextBox79.Text = String.Format("{0:F3}", PechValues2.txtcprq);
            TextBox80.Text = String.Format("{0:F3}", PechValues2.txtq5tst);
            TextBox81.Text = String.Format("{0:F3}", TextBox73.Text);
            TextBox82.Text = String.Format("{0:F3}", PechValues2.tst2);
            TextBox83.Text = String.Format("{0:F3}", PechValues2.txtfsv);
            TextBox84.Text = String.Format("{0:F3}", PechValues2.txtfsvv);
            TextBox85.Text = String.Format("{0:F3}", PechValues2.txtq5tsv);
            TextBox86.Text = String.Format("{0:F3}", PechValues2.txtfdv);
            TextBox87.Text = String.Format("{0:F3}", PechValues2.txtq5dtv);
            TextBox88.Text = String.Format("{0:F3}", PechValues2.txtq5trp);
            TextBox89.Text = String.Format("{0:F3}", PechValues2.txtq5rp);
            TextBox90.Text = String.Format("{0:F3}", PechValues2.t0nch);
            TextBox91.Text = String.Format("{0:F3}", PechValues2.t1nch);
            TextBox92.Text = String.Format("{0:F3}", PechValues2.txttdashstn);
            TextBox93.Text = String.Format("{0:F3}", PechValues2.txttdashst1);
            TextBox94.Text = String.Format("{0:F3}", PechValues2.txttdashst2);
            TextBox95.Text = String.Format("{0:F3}", PechValues2.txttdashslice1);
            TextBox96.Text = String.Format("{0:F3}", PechValues2.txttdashslice2);
            TextBox97.Text = String.Format("{0:F3}", PechValues2.txttdashngr);
            TextBox98.Text = String.Format("{0:F3}", PechValues2.txttdashohl);
            TextBox99.Text = String.Format("{0:F3}", PechValues2.txttdashstd);
            TextBox100.Text = String.Format("{0:F3}", PechValues2.txttbigcom);
            TextBox101.Text = String.Format("{0:F3}", PechValues2.txttbigcom2);
            TextBox102.Text = String.Format("{0:F3}", PechValues2.txttdashstngr);
            TextBox103.Text = String.Format("{0:F3}", PechValues2.txtfbigsum);
            TextBox104.Text = String.Format("{0:F3}", PechValues2.txtqbig6);
            TextBox105.Text = String.Format("{0:F3}", PechValues2.txtkpdpech);
            TextBox106.Text = String.Format("{0:F3}", PechValues2.txtqbigprih);
            TextBox107.Text = String.Format("{0:F3}", "100");
            TextBox108.Text = String.Format("{0:F3}", PechValues2.txtqbig1);
            TextBox109.Text = String.Format("{0:F3}", PechValues2.txtqbig1percent);
            TextBox110.Text = String.Format("{0:F3}", PechValues2.txtqbig2);
            TextBox111.Text = String.Format("{0:F3}", PechValues2.txtqbig22percent);
            TextBox112.Text = String.Format("{0:F3}", PechValues2.txtq5rpsum);
            TextBox113.Text = String.Format("{0:F3}", PechValues2.txtq5rpsumpercent);
            TextBox114.Text = String.Format("{0:F3}", PechValues2.txtqbig6);
            TextBox115.Text = String.Format("{0:F3}", PechValues2.txtqbig6percent);
            TextBox116.Text = String.Format("{0:F3}", PechValues2.txtqbigsum);
            TextBox117.Text = String.Format("{0:F3}", PechValues2.txtqbigsumpercent);
        }

        protected void ButtonH_Click(object sender, EventArgs e)
        {
            this.LabelH.Visible = true;
        }
    }
}