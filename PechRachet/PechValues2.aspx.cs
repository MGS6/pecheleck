﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using PechLib;
using System.Web.Helpers;

namespace PechRachet
{
    public partial class PechValues2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        double qoa, lbig, dbig, hz, k3, d0, notv, m, tpol, kf, t2dash, w1c, w2c, tm2, qbig, qbig2, q5pr, cprq, t0dash, t1dash, tst1, sbig, f1big, f2big, fbigsr, q5tst, fsv, fsvv, q5tsv, fdv, q5dtv, q5trp, q5rp, tdashstn, tdashst1, tdashst2, tdashslice1, tdashslice2, tdashngr, tdashohl, tdashstd, tbigcom, tbigcom2, tdashstngr, fbigsum, qbig6, qbig1, qbig1percent, qbig22, qbig22percent, q5rpsum, q5rpsumpercent, qbig6percent, qbigsum, qbigsumpercent, qbigprih, kpdpech;
        double q11, q21, q31, beta1, az1, bz1, cz1, gm1, mop1, mtel1, w11, w21, tm11, t11, tsh1, tl1, tll1, tlll1, ak1, bk1, fk1, hk1, ik1, uk1, t0n1, t0l1, t0ll1, t1n1, t1l1, t1ll1, t2n1, t2l1, t2ll1, pkriv1, tpr11, tpr21, tsl1, tvn1, tst21, cm1, cvl1, cpp1, ip1001, i1vl1, vb1, zeta1, tlambdad, tlambda11, tlambda21, sss11, sss21, tlambdat1, sdv1, tlambdadv1, c91, p91;
        public static string tst2, t1nch, t0nch, txtqoa, txtlbig, txtdbig, txthz, txtk3, txtd0, txtnotv, txtm, txttpol, txtkf, txtt2dash, txtw1c, txtw2c, txttm2, txtqbig, txtqbig2, txtq5pr, txtcprq, txtfsv, txtfsvv, txtt0dash, txtt1dash, txttst1, txtsbig, txtq5tsv, txtf1big, txtf2big, txtfbigsr, txtq5tst, txtfdv, txtq5dtv, txtq5trp, txtq5rp, txttdashstn, txttdashst1, txttdashst2, txttdashslice1, txttdashslice2, txttdashngr, txttdashohl, txttdashstd, txttbigcom, txttbigcom2, txttdashstngr, txtfbigsum, txtqbig6, txtqbig1, txtqbig1percent, txtqbig22, txtqbig22percent, txtq5rpsum, txtq5rpsumpercent, txtqbig6percent, txtqbigsumpercent, txtqbigprih, txtkpdpech, txtqbigsum, txt55, txt56, txt57, txt58, txt59, txt60, txt61, txt62;


        protected void Button1_Click(object sender, EventArgs e)
        {

            Directory.CreateDirectory(Path.GetDirectoryName(@"C:\FR\pechval2.xml"));
            XmlTextWriter writer = new XmlTextWriter(@"C:\FR\pechval2.xml", System.Text.Encoding.UTF8);
            writer.WriteStartDocument(true);
            writer.Formatting = Formatting.Indented;
            writer.Indentation = 2;
            writer.WriteStartElement("list");
            newNode("q1", TextBox1.Text, writer);
            newNode("q2", TextBox2.Text, writer);
            newNode("q3", TextBox3.Text, writer);
            newNode("beta", TextBox56.Text, writer);
            newNode("az", TextBox4.Text, writer);
            newNode("bz", TextBox5.Text, writer);
            newNode("cz", TextBox6.Text, writer);
            newNode("gm", TextBox7.Text, writer);
            newNode("mop", TextBox8.Text, writer);
            newNode("mtel", TextBox9.Text, writer);
            newNode("w1", TextBox10.Text, writer);
            newNode("w2", TextBox11.Text, writer);
            newNode("tm1", TextBox12.Text, writer);
            newNode("t1", TextBox13.Text, writer);
            newNode("tsh", TextBox14.Text, writer);
            newNode("tl", TextBox15.Text, writer);
            newNode("tll", TextBox16.Text, writer);
            newNode("tlll", TextBox17.Text, writer);
            newNode("ak", TextBox18.Text, writer);
            newNode("bk", TextBox19.Text, writer);
            newNode("fk", TextBox20.Text, writer);
            newNode("hk", TextBox21.Text, writer);
            newNode("ik", TextBox22.Text, writer);
            newNode("uk", TextBox23.Text, writer);
            newNode("t0n", TextBox24.Text, writer);
            newNode("t0l", TextBox25.Text, writer);
            newNode("t0ll", TextBox26.Text, writer);
            newNode("t1n", TextBox26b.Text, writer);
            newNode("t1l", TextBox27.Text, writer);
            newNode("t1ll", TextBox28.Text, writer);
            newNode("t2n", TextBox29.Text, writer);
            newNode("t2l", TextBox30.Text, writer);
            newNode("t2ll", TextBox31.Text, writer);
            newNode("pkriv", TextBox32.Text, writer);
            newNode("tpr1", TextBox33.Text, writer);
            newNode("tpr2", TextBox34.Text, writer);
            newNode("tsl", TextBox35.Text, writer);
            newNode("tvn", TextBox36.Text, writer);
            newNode("tst2", TextBox37.Text, writer);
            newNode("cm", TextBox38.Text, writer);
            newNode("cvl", TextBox39.Text, writer);
            newNode("cpp", TextBox40.Text, writer);
            newNode("ip100", TextBox41.Text, writer);
            newNode("i1vl", TextBox42.Text, writer);
            newNode("vb", TextBox43.Text, writer);
            newNode("zeta", TextBox44.Text, writer);
            newNode("tlambdad", TextBox45.Text, writer);
            newNode("tlambda1", TextBox46.Text, writer);
            newNode("tlambda2", TextBox47.Text, writer);
            newNode("sss1", TextBox48.Text, writer);
            newNode("sss2", TextBox49.Text, writer);
            newNode("tlambdat", TextBox50.Text, writer);
            newNode("sdv", TextBox51.Text, writer);
            newNode("lambdadv", TextBox52.Text, writer);
            newNode("c9", TextBox53.Text, writer);
            newNode("p9", TextBox54.Text, writer);
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();


            q11 = double.Parse(TextBox1.Text);
            q21 = double.Parse(TextBox2.Text);
            q31 = double.Parse(TextBox3.Text);
            beta1 = double.Parse(TextBox56.Text);
            az1 = double.Parse(TextBox4.Text);
            bz1 = double.Parse(TextBox5.Text);
            cz1 = double.Parse(TextBox6.Text);
            gm1 = double.Parse(TextBox7.Text);
            mop1 = double.Parse(TextBox8.Text);
            mtel1 = double.Parse(TextBox9.Text);
            w11 = double.Parse(TextBox10.Text);
            w21 = double.Parse(TextBox11.Text);
            tm11 = double.Parse(TextBox12.Text);
            t11 = double.Parse(TextBox13.Text);
            tsh1 = double.Parse(TextBox14.Text);
            tl1 = double.Parse(TextBox15.Text);
            tll1 = double.Parse(TextBox16.Text);
            tlll1 = double.Parse(TextBox17.Text);
            ak1 = double.Parse(TextBox18.Text);
            bk1 = double.Parse(TextBox19.Text);
            fk1 = double.Parse(TextBox20.Text);
            hk1 = double.Parse(TextBox21.Text);
            ik1 = double.Parse(TextBox22.Text);
            uk1 = double.Parse(TextBox23.Text);
            t0n1 = double.Parse(TextBox24.Text);
            t0l1 = double.Parse(TextBox25.Text);
            t0ll1 = double.Parse(TextBox26.Text);
            t1n1 = double.Parse(TextBox26b.Text);
            t1l1 = double.Parse(TextBox27.Text);
            t1ll1 = double.Parse(TextBox28.Text);
            t2n1 = double.Parse(TextBox29.Text);
            t2l1 = double.Parse(TextBox30.Text);
            t2ll1 = double.Parse(TextBox31.Text);
            pkriv1 = double.Parse(TextBox32.Text);
            tpr11 = double.Parse(TextBox33.Text);
            tpr21 = double.Parse(TextBox34.Text);
            tsl1 = double.Parse(TextBox35.Text);
            tvn1 = double.Parse(TextBox36.Text);
            tst21 = double.Parse(TextBox37.Text);
            cm1 = double.Parse(TextBox38.Text);
            cvl1 = double.Parse(TextBox39.Text);
            cpp1 = double.Parse(TextBox40.Text);
            ip1001 = double.Parse(TextBox41.Text);
            i1vl1 = double.Parse(TextBox42.Text);
            vb1 = double.Parse(TextBox43.Text);
            zeta1 = double.Parse(TextBox44.Text);
            tlambdad = double.Parse(TextBox45.Text);
            tlambda11 = double.Parse(TextBox46.Text);
            tlambda21 = double.Parse(TextBox47.Text);
            sss11 = double.Parse(TextBox48.Text);
            sss21 = double.Parse(TextBox49.Text);
            tlambdat1 = double.Parse(TextBox50.Text);
            sdv1 = double.Parse(TextBox51.Text);
            tlambdadv1 = double.Parse(TextBox52.Text);
            c91 = double.Parse(TextBox53.Text);
            p91 = double.Parse(TextBox54.Text);

            txtqoa = PechClass.qoa(q11, q21, q31).ToString("0");
            qoa = double.Parse(txtqoa);
            txtlbig = PechClass.lbig(az1, q11, fk1).ToString("0");
            lbig = double.Parse(txtlbig);
            txtdbig = PechClass.dbig(bz1, q21, ak1).ToString("0.000");
            dbig = double.Parse(txtdbig);
            txthz = PechClass.hz(q31, cz1, bk1, hk1, uk1).ToString("0.00");
            hz = double.Parse(txthz);
            txtk3 = PechClass.k3(az1, bz1, cz1, qoa, hz, lbig, dbig).ToString("0.00");
            k3 = double.Parse(txtk3);
            txtd0 = PechClass.d0(ak1, hz, uk1, beta1).ToString("0.000");
            d0 = double.Parse(txtd0);
            txtnotv = PechClass.notv(lbig, d0, hz, uk1, beta1).ToString("0");
            notv = double.Parse(txtnotv);
            txtm = PechClass.m(ik1, d0).ToString("0.000");
            m = double.Parse(txtm);
            txttpol = PechClass.tpol(t11, t0l1).ToString("0");
            tpol = double.Parse(txttpol);
            txtkf = PechClass.kf(tpol).ToString("0.00");
            kf = double.Parse(txtkf);
            txtt2dash = PechClass.t2dash(t2n1, t2l1, pkriv1, tl1, tll1, t2ll1).ToString("0");
            t2dash = double.Parse(txtt2dash);
            txtw1c = PechClass.w1c(w11).ToString("0.00");
            w1c = double.Parse(txtw1c);
            txtw2c = PechClass.w2c(w21).ToString("0.00");
            w2c = double.Parse(txtw2c);
            txttm2 = PechClass.tm2(t1ll1, t2ll1).ToString("0");
            tm2 = double.Parse(txttm2);
            txtqbig = PechClass.qbig(cm1, w2c, cvl1, tm11, w1c, ip1001, i1vl1, t2dash, gm1, tm2, cpp1).ToString("0");
            qbig = double.Parse(txtqbig);
            txtqbig2 = PechClass.qbig2(t2l1, tl1, vb1, t2ll1, tll1).ToString("0.00");
            qbig2 = double.Parse(txtqbig2);
            txtcprq = PechClass.cprq(t11).ToString("0.0000");
            cprq = double.Parse(txtcprq);
            txtq5pr = PechClass.q5pr(cprq, tpr21, tpr11, mop1, mtel1).ToString("0");
            q5pr = double.Parse(txtq5pr);
            txtt0dash = PechClass.t0dash(t0n1, t0l1, pkriv1, tl1, tll1, t0ll1).ToString("0");
            t0dash = double.Parse(txtt0dash);
            txtt1dash = PechClass.t1dash(t1n1, t1l1, pkriv1, tl1, t1ll1, tll1).ToString("0");
            t1dash = double.Parse(txtt1dash);
            txttst1 = PechClass.tst1(t0dash, t1dash).ToString("0");
            tst1 = double.Parse(txttst1);
            txtsbig = PechClass.sbig(zeta1).ToString("0.00");
            sbig = double.Parse(txtsbig);
            txtf1big = PechClass.f1big(lbig, hz, dbig).ToString("0.0");
            f1big = double.Parse(txtf1big);
            txtf2big = PechClass.f2big(lbig, sbig, hz, dbig).ToString("0.0");
            f2big = double.Parse(txtf2big);
            txtfbigsr = PechClass.fbigsr(f1big, f2big).ToString("0.0");
            fbigsr = double.Parse(txtfbigsr);
            txtq5tst = PechClass.q5tst(tlambdad, sbig, tst1, tst21, fbigsr, tsh1).ToString("0");
            q5tst = double.Parse(txtq5tst);
            txtfsv = PechClass.fsv(lbig, sbig, dbig).ToString("0.0");
            fsv = double.Parse(txtfsv);
            txtfsvv = PechClass.fsvv(lbig, dbig).ToString("0.0");
            fsvv = double.Parse(txtfsvv);
            txtq5tsv = PechClass.q5tsv(t1dash, tst21, fsv, tsh1, sss11, tlambda11, sss21, tlambda21).ToString("0");
            q5tsv = double.Parse(txtq5tsv);
            txtfdv = PechClass.fdv(dbig, sbig, hz).ToString("0.0");
            fdv = double.Parse(txtfdv);
            txtq5dtv = PechClass.q5dtv(t1dash, tst21, fdv, tsh1, sdv1, tlambdat1).ToString("0");
            q5dtv = double.Parse(txtq5dtv);
            txtq5trp = PechClass.q5trp(q5dtv, q5tsv, q5tst).ToString("0");
            q5trp = double.Parse(txtq5trp);
            txtq5rp = PechClass.q5rp(q5trp, q5pr).ToString("0");
            q5rp = double.Parse(txtq5rp);
            txttdashstn = PechClass.tdashstn(tvn1, t0n1, t1n1).ToString("0.0");
            tdashstn = double.Parse(txttdashstn);
            txttdashst1 = PechClass.tdashst1(t0l1, t1l1).ToString("0");
            tdashst1 = double.Parse(txttdashst1);
            txttdashst2 = PechClass.tdashst2(t0ll1, t1ll1).ToString("0");
            tdashst2 = double.Parse(txttdashst2);
            txttdashslice1 = PechClass.tdashslice1(tdashst1, tdashstn).ToString("0.00");
            tdashslice1 = double.Parse(txttdashslice1);
            txttdashslice2 = PechClass.tdashslice2(tdashst1, tdashst2, tdashstn).ToString("0.00");
            tdashslice2 = double.Parse(txttdashslice2);
            txttdashngr = PechClass.tdashngr(tl1, tll1, tdashslice1, tdashslice2, tsh1).ToString("0.000");
            tdashngr = double.Parse(txttdashngr);
            txttdashohl = PechClass.tdashohl(tdashst2, tdashstn).ToString("0");
            tdashohl = double.Parse(txttdashohl);
            txttdashstd = PechClass.tdashstd(tl1, tdashslice1, tll1, tdashslice2, tsh1).ToString("0.000");
            tdashstd = double.Parse(txttdashstd);
            txttbigcom = PechClass.tbigcom(tsh1, tdashngr, tdashstd).ToString("0");
            tbigcom = double.Parse(txttbigcom);
            txttbigcom2 = PechClass.tbigcom2(tlll1, tdashohl, tdashstd).ToString("0");
            tbigcom2 = double.Parse(txttbigcom2);
            txttdashstngr = PechClass.tdashstngr(tdashstn, tdashngr).ToString("0.000");
            tdashstngr = double.Parse(txttdashstngr);
            txtfbigsum = PechClass.fbigsum(f1big, fdv, fsvv).ToString("0.0");
            fbigsum = double.Parse(txtfbigsum);
            txtqbig6 = PechClass.qbig6(tlambdadv1, c91, p91, tbigcom, tbigcom2, tdashstd, fbigsum).ToString("0");
            qbig6 = double.Parse(txtqbig6);
            txtqbig1 = PechClass.qbig1(cm1, w2c, cvl1, tm11, w1c, ip1001, i1vl1, t2dash, gm1, tm2, cpp1).ToString("0");
            qbig1 = double.Parse(txtqbig1);
            txtqbig22 = PechClass.qbig22(t2l1, tl1, vb1, t2ll1, tll1).ToString("0");
            qbig22 = double.Parse(txtqbig22);
            txtq5rpsum = PechClass.q5rpsum(q5trp, q5pr).ToString("0");
            q5rpsum = double.Parse(txtq5rpsum);
            txtqbigprih = PechClass.qbigprih(qbig1, qbig22, q5rpsum, qbig6).ToString("0");
            qbigprih = double.Parse(txtqbigprih);
            txtqbig1percent = PechClass.qbig1percent(qbig1, qbigprih).ToString("0.0");
            qbig1percent = double.Parse(txtqbig1percent);
            txtqbig22percent = PechClass.qbig22percent(qbig22, qbigprih).ToString("0.0");
            qbig22percent = double.Parse(txtqbig22percent);
            txtq5rpsumpercent = PechClass.q5rpsumpercent(q5rpsum, qbigprih).ToString("0.0");
            q5rpsumpercent = double.Parse(txtq5rpsumpercent);
            txtqbig6percent = PechClass.qbig6percent(qbig6, qbigprih).ToString("0.0");
            qbig6percent = double.Parse(txtqbig6percent);
            txtqbigsum = PechClass.qbigsum(qbig1, qbig22, q5rpsum, qbig6).ToString("0");
            qbigsum = double.Parse(txtqbigsum);
            txtqbigsumpercent = PechClass.qbigsumpercent(qbig1percent, qbig22percent, q5rpsumpercent, qbig6percent).ToString("0.0");
            qbigsumpercent = double.Parse(txtqbigsumpercent);
            txtkpdpech = PechClass.kpdpech(qbig1, qbigprih).ToString("0.");
            kpdpech = double.Parse(txtkpdpech);
            tst2 = TextBox37.Text;
            t0nch = TextBox24.Text;
            t1nch = TextBox26b.Text;

            XmlTextWriter writersave = new XmlTextWriter(@"C:\FR\pechresult2.xml", System.Text.Encoding.UTF8);
            writersave.WriteStartDocument(true);
            writersave.Formatting = Formatting.Indented;
            writersave.Indentation = 2;
            writersave.WriteStartElement("list");
            newNodeSave("qoa", txtqoa, writersave);
            newNodeSave("lbig", txtlbig, writersave);
            newNodeSave("dbig", txtdbig, writersave);
            newNodeSave("hz", txthz, writersave);
            newNodeSave("k3", txtk3, writersave);
            newNodeSave("d0", txtd0, writersave);
            newNodeSave("notv", txtnotv, writersave);
            newNodeSave("m", txtm, writersave);
            newNodeSave("tpol", txttpol, writersave);
            newNodeSave("kf", txtkf, writersave);
            newNodeSave("t2dash", txtt2dash, writersave);
            newNodeSave("w1c", txtw1c, writersave);
            newNodeSave("w2c", txtw2c, writersave);
            newNodeSave("tm2", txttm2, writersave);
            newNodeSave("qbig", txtqbig, writersave);
            newNodeSave("qbig2", txtqbig2, writersave);
            newNodeSave("q5pr", txtq5pr, writersave);
            newNodeSave("cprq", txtcprq, writersave);
            newNodeSave("t0dash", txtt0dash, writersave);
            newNodeSave("t1dash", txtt1dash, writersave);
            newNodeSave("tst1", txttst1, writersave);
            newNodeSave("sbig", txtsbig, writersave);
            newNodeSave("f1big", txtf1big, writersave);
            newNodeSave("f2big", txtf2big, writersave);
            newNodeSave("fbigsr", txtfbigsr, writersave);
            newNodeSave("q5tst", txtq5tst, writersave);
            newNodeSave("fsv", txtfsv, writersave);
            newNodeSave("fsvv", txtfsvv, writersave);
            newNodeSave("q5tsv", txtq5tsv, writersave);
            newNodeSave("fdv", txtfdv, writersave);
            newNodeSave("q5dtv", txtq5dtv, writersave);
            newNodeSave("q5trp", txtq5trp, writersave);
            newNodeSave("q5rp", txtq5rp, writersave);
            newNodeSave("tdashstn", txttdashstn, writersave);
            newNodeSave("tdashst1", txttdashst1, writersave);
            newNodeSave("tdashst2", txttdashst2, writersave);
            newNodeSave("tdashslice1", txttdashslice1, writersave);
            newNodeSave("tdashslice2", txttdashslice2, writersave);
            newNodeSave("tdashngr", txttdashngr, writersave);
            newNodeSave("tdashohl", txttdashohl, writersave);
            newNodeSave("tdashstd", txttdashstd, writersave);
            newNodeSave("tbigcom", txttbigcom, writersave);
            newNodeSave("tbigcom2", txttbigcom2, writersave);
            newNodeSave("tdashstngr", txttdashstngr, writersave);
            newNodeSave("fbigsum", txtfbigsum, writersave);
            newNodeSave("qbig6", txtqbig6, writersave);
            newNodeSave("qbig1", txtqbig1, writersave);
            newNodeSave("qbig1percent", txtqbig1percent, writersave);
            newNodeSave("qbig2", txtqbig2, writersave);
            newNodeSave("qbig2percent", txtqbig22percent, writersave);
            newNodeSave("q5rpsum", txtq5rpsum, writersave);
            newNodeSave("q5rpsumpercent", txtq5rpsumpercent, writersave);
            newNodeSave("qbig6", txtqbig6, writersave);
            newNodeSave("qbig6percent", txtqbig6percent, writersave);
            newNodeSave("qbigsum", txtqbigsum, writersave);
            newNodeSave("qbigsumpercent", txtqbigsumpercent, writersave);
            newNodeSave("qbigprih", txtqbigprih, writersave);
            newNodeSave("kpdpech", txtkpdpech, writersave);
            writersave.WriteEndElement();
            writersave.WriteEndDocument();
            writersave.Close();

            Response.Redirect("PechResults2.aspx");

        }

        private const string XML_FILE_NAME = "pechval2.xml";
        public static XmlDocument LoadSampleXML()
        {
            XmlDocument doc = null;
            try
            {
                doc = new XmlDocument();
                doc.Load(XML_FILE_NAME);
                return doc;
            }
            catch (Exception ex)
            {
                return doc;
            }
        }


        private void newNode(string vname, string vvalue, XmlTextWriter writer)
        {
            writer.WriteStartElement("listitem");
            writer.WriteStartElement("varname");
            writer.WriteString(vname);
            writer.WriteEndElement();
            writer.WriteStartElement("varvalue");
            writer.WriteString(vvalue);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        private void newNodeSave(string vname, string vvalue, XmlTextWriter writer)
        {
            writer.WriteStartElement("listitem");
            writer.WriteStartElement("varname");
            writer.WriteString(vname);
            writer.WriteEndElement();
            writer.WriteStartElement("varvalue");
            writer.WriteString(vvalue);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

    }
}