﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using PechLib;


namespace PechRachet
{
    public partial class PechValues : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private const string XML_FILE_NAME = "pechval.xml";
        public static XmlDocument LoadSampleXML()
        {
            XmlDocument doc = null;
            try
            {
                doc = new XmlDocument();
                doc.Load(XML_FILE_NAME);
                return doc;
            }
            catch (Exception ex)
            {
                return doc;
            }
        }

        double bc, ad, pt, vb2, re, lambd, aev, tn, pn, rf, ie, ds, h, le, cp, ivo, ab;
        double a1, b1, c1, d1, ww1, dr1, u1;
        public static string txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8, txt9, txt10, txt11, txt12, txt13, txt14, txt15, txt16;


        protected void Button2_Click(object sender, EventArgs e)
        {
            Directory.CreateDirectory(Path.GetDirectoryName(@"C:\FR\pechval.xml"));
            XmlTextWriter writer = new XmlTextWriter(@"C:\FR\pechval.xml", System.Text.Encoding.UTF8);
            writer.WriteStartDocument(true);
            writer.Formatting = Formatting.Indented;
            writer.Indentation = 2;
            writer.WriteStartElement("list");
            newNode("a", TextBox1.Text, writer);
            newNode("b", TextBox2.Text, writer);
            newNode("c", TextBox3.Text, writer);
            newNode("d", TextBox4.Text, writer);
            newNode("ww", TextBox5.Text, writer);
            newNode("dr", TextBox6.Text, writer);
            newNode("u", TextBox7.Text, writer);
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();

            a1 = double.Parse(TextBox1.Text);
            b1 = double.Parse(TextBox2.Text);
            c1 = double.Parse(TextBox3.Text);
            d1 = double.Parse(TextBox4.Text);
            ww1 = double.Parse(TextBox5.Text);
            dr1 = double.Parse(TextBox6.Text);
            u1 = double.Parse(TextBox7.Text);
            double v1 = 0.00000000006;
            double v2 = 0.0000001;
            double v3 = 0.00001;

            txt1 = PechClass.bc(b1, c1).ToString("0");
            bc = double.Parse(txt1);
            txt2 = PechClass.ad(a1, d1).ToString("0");
            ad = double.Parse(txt2);
            txt3 = PechClass.pt(bc).ToString("0.000");
            pt = double.Parse(txt3);
            txt4 = PechClass.vb2(bc, v1, v2, v3).ToString("0.000000");
            vb2 = double.Parse(txt4);
            txt5 = PechClass.re(ww1, dr1, vb2).ToString("0.000");
            re = double.Parse(txt5);
            txt6 = PechClass.lambd(bc).ToString("0.0000");
            lambd = double.Parse(txt6);
            txt7 = PechClass.aev(re, dr1, lambd).ToString("0.000");
            aev = double.Parse(txt7);
            txt8 = PechClass.tn(bc, pt, ad, u1, aev, dr1).ToString("0.000");
            tn = double.Parse(txt8);
            txt9 = PechClass.pn(tn).ToString("0.000");
            pn = double.Parse(txt9);
            txt10 = PechClass.rf(u1, ad).ToString("0.000");
            rf = double.Parse(txt10);
            txt11 = PechClass.ie(rf, dr1, pn).ToString("0.000");
            ie = double.Parse(txt11);
            txt12 = PechClass.ds(dr1).ToString("0");
            ds = double.Parse(txt12);
            txt13 = PechClass.h(dr1).ToString("0");
            h = double.Parse(txt13);
            txt14 = PechClass.le(ie, ds, h).ToString("0.00");
            txt15 = PechClass.cp(bc).ToString("0.0000");
            cp = double.Parse(txt15);
            txt16 = PechClass.ivo(cp, b1).ToString("0.000000");

            XmlTextWriter writersave = new XmlTextWriter(@"C:\FR\pechresult.xml", System.Text.Encoding.UTF8);
            writersave.WriteStartDocument(true);
            writersave.Formatting = Formatting.Indented;
            writersave.Indentation = 2;
            writersave.WriteStartElement("list");
            newNodeSave("bc", txt1, writersave);
            newNodeSave("ad", txt2, writersave);
            newNodeSave("pt", txt3, writersave);
            newNodeSave("vb", txt4, writersave);
            newNodeSave("re", txt5, writersave);
            newNodeSave("lambd", txt6, writersave);
            newNodeSave("aev", txt7, writersave);
            newNodeSave("tr", txt8, writersave);
            newNodeSave("pn", txt9, writersave);
            newNodeSave("rf", txt10, writersave);
            newNodeSave("ie", txt11, writersave);
            newNodeSave("ds", txt12, writersave);
            newNodeSave("h", txt13, writersave);
            newNodeSave("le", txt14, writersave);
            newNodeSave("cp", txt15, writersave);
            newNodeSave("ivo", txt16, writersave);
            writersave.WriteEndElement();
            writersave.WriteEndDocument();
            writersave.Close();

            Response.Redirect("PechResults.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("PechValues2.aspx");
        }

        private void newNode(string vname, string vvalue, XmlTextWriter writer)
        {
            writer.WriteStartElement("listitem");
            writer.WriteStartElement("varname");
            writer.WriteString(vname);
            writer.WriteEndElement();
            writer.WriteStartElement("varvalue");
            writer.WriteString(vvalue);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        private void newNodeSave(string vname, string vvalue, XmlTextWriter writersave)
        {
            writersave.WriteStartElement("listitem");
            writersave.WriteStartElement("varname");
            writersave.WriteString(vname);
            writersave.WriteEndElement();
            writersave.WriteStartElement("varvalue");
            writersave.WriteString(vvalue);
            writersave.WriteEndElement();
            writersave.WriteEndElement();
        }
    }
}